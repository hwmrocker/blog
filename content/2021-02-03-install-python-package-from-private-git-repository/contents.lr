title: Install Pythonpackage from private git repository
---
body:

#### 1. setup privat libary repo

1.1. setup deploy key

```bash
$ ssh-keygen -t ed25519 -C "deploy_key_library" -f /tmp/deploy

$ cat /tmp/deploy.pub
```

1.2. add deploy key to github
https://github.com/hwmrocker/test_private_library/deploy_keys

#### 2. setup app repo

2.1 add private key to github

```bash
$ cat /tmp/deploy
```
https://github.com/hwmrocker/test_action_build/settings/secrets/actions/new

as `TEST_LIB_DEPLOY_KEY`

2.2 add test

```python
from test_private_library import random


def test_random():
    assert random() == 4
```

2.3 define the requirements.txt

```
git+ssh://git@github.com/hwmrocker/test_private_library.git#egg=test_private_library
pytest
```

2.4 add github action that installs the library

```yaml
name: PyTest
on: push

jobs:
  test:
    runs-on: ubuntu-latest
    timeout-minutes: 10

    steps:
      - name: Check out repository code
        uses: actions/checkout@v2

      # Setup Python (faster than using Python container)
      - name: Setup Python
        uses: actions/setup-python@v2
        with:
          python-version: "3.10"

      - name: Install dependencies
        env:
          DEPLOY_KEY: ${{secrets.TEST_LIB_DEPLOY_KEY}}
        run: |
          eval "$(ssh-agent -s)"
          echo "${DEPLOY_KEY}" | tr -d '\r' | ssh-add -
          mkdir -p ~/.ssh
          chmod 700 ~/.ssh

          pip install -r requirements.txt

      - name: Run tests
        run: |
          pytest
```

You can load the ssh key with those lines:

```yaml
env:
    DEPLOY_KEY: ${{secrets.TEST_LIB_DEPLOY_KEY}}
run: |
    eval "$(ssh-agent -s)"
    echo "${DEPLOY_KEY}" | tr -d '\r' | ssh-add -
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
```

You can't run it seperately, you need to run the whole thing in one action where you install the dependencies.

---
pub_date: 2021-02-03
---
tags:

python
python3
github
